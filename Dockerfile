FROM centos:6.8
#RUN yum -y install httpd mod_ssl openssl nc && \
#yum clean all && \
#chkconfig httpd on
COPY rpm /tmp
COPY storage /storage

RUN yum -y upgrade && yum -y install epel-release glibc.i686 libgcc.i686 libstdc++.i686

RUN rpm -ihv /tmp/1C_Enterprise83-common-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-common-nls-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-server-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-server-nls-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-ws-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-ws-nls-8.3.7-2027.i386.rpm && \
rpm -ihv /tmp/1C_Enterprise83-crs-8.3.7-2027.i386.rpm && \
rm -rf /tmp && \
mkdir /tmp && chmod 777 /tmp

EXPOSE 1542
ENTRYPOINT ["/opt/1C/v8.3/i386/crserver", "-d", "/storage"]

